var trigo = {
        codes: {
            OK: "EVERYTHING_OKAY",
            ERROR: "WTF???",
            NOT_FINISHED: "NOT_FINISHED!!!!"
        },
        inputs: [],
        values: {
            a: 0.0,
            b: 0.0,
            c: 0.0,
            ac: 0.0,
            bc: 0.0,
            cc: 0.0,

            ca: 0.0,
            cb: 0.0,

            height: 0.0,
            width: 0.0,

            flaeche: 0.0,
            umfang: 0.0,

            g: [],
            h: []
        },
        init: function () {
            for (var i = 0; i < 15; i++) {
                trigo.inputs[i] = document.getElementsByName("Text" + i)[0];
            }
            trigo.renderer.init();
        },
        clear: function () {
            for (var i = 0; i < 15; i++) {
                trigo.inputs[i].value = "";
            }
            trigo.values.g = [];
            trigo.values.h = [];

            trigo.renderer.clear();

            trigo.inputs[0].focus();
        },
        change: function (Index) {
            if (Index > 5 || Index === -1) return;
            if (Index > 2) {
                trigo.inputs[Index + 6].value = trigo.inputs[Index].value + '°';
            } else {
                trigo.inputs[Index + 6].value = trigo.inputs[Index].value;
            }

            trigo.values.g[Index] = trigo.inputs[Index].value !== "";
            trigo.renderer.clear();


            var t;
            var i = 0;
            var w = 0.0;
            t = trigo.inputs[Index].value.replace(/\,/g, ".").replace(/ /g, "").replace(/°/, "");

            // t = t.;
            if (t.charAt(0) === ",") t = "0" + t;
            w = Number(t);
            if ((isNaN(w)) || (t === "")) {
                trigo.inputs[Index].value = "";
                return;
            }
            tt = new Boolean, n = 0;
            switch (Index) {
                case 0:
                    trigo.values.a = w;
                    break;
                case 1:
                    trigo.values.b = w;
                    break;
                case 2:
                    trigo.values.c = w;
                    break;
                case 3:
                    trigo.values.ac = w;
                    break;
                case 4:
                    trigo.values.bc = w;
                    break;
                case 5:
                    trigo.values.cc = w;
                    break;
                default:
            }
            if (Index < 6) {
                for (i = 0; i < 6; i++) {
                    trigo.inputs[i + 6].value = trigo.inputs[i].value;
                    tt = trigo.inputs[i].value !== "";
                    if (tt) {
                        trigo.values.g[i] = 1;
                        trigo.values.h[i] = 1;
                    } else {
                        trigo.values.g[i] = 0;
                        trigo.values.h[i] = 0;
                    }
                    if (tt) n++;
                }
                if (n > 3) {
                    trigo.msg.print("Nur drei Eingaben zulässig!");
                    trigo.inputs[Index].value = "";
                    trigo.inputs[Index + 6].value = "";
                    trigo.inputs[12].value = "";
                    trigo.inputs[13].value = "";
                    trigo.inputs[14].value = "";
                    trigo.change(Index);
                    return;
                }
                trigo.inputs[12].value = "";
                trigo.inputs[13].value = "";
                trigo.inputs[14].value = "";
                var e = trigo.codes.ERROR;
                do {
                    e = trigo.calc.berechnen();
                    var finished = 6;
                    if (trigo.values.h[0] === 1) {
                        trigo.inputs[6].value = trigo.calc.runden(trigo.values.a, 4);
                        finished--;
                    }
                    if (trigo.values.h[1] === 1) {
                        trigo.inputs[7].value = trigo.calc.runden(trigo.values.b, 4);
                        finished--;
                    }
                    if (trigo.values.h[2] === 1) {
                        trigo.inputs[8].value = trigo.calc.runden(trigo.values.c, 4);
                        finished--;
                    }
                    if (trigo.values.h[3] === 1) {
                        trigo.inputs[9].value = trigo.calc.runden(trigo.values.ac, 4) + "°";
                        finished--;
                    }
                    if (trigo.values.h[4] === 1) {
                        trigo.inputs[10].value = trigo.calc.runden(trigo.values.bc, 4) + "°";
                        finished--;
                    }
                    if (trigo.values.h[5] === 1) {
                        trigo.inputs[11].value = trigo.calc.runden(trigo.values.cc, 4) + "°";
                        console.log(trigo.values.cc);
                        finished--;
                    }

                    if(e === trigo.codes.NOT_FINISHED){
                        break;
                    }else if(e === trigo.codes.ERROR){
                        // trigo.msg.print("Fehler Aufgetreten!");
                        break;
                    }else if (e === trigo.codes.OK) {
                        if(finished === 0){
                            break;
                        }
                    }else{
                        console.log(e);
                        trigo.msg.print("WTF is e??");
                    }
                } while (e === trigo.codes.OK);
                if (e === trigo.codes.OK) {
                    trigo.calc.restBerechnen();
                    trigo.inputs[12].value = trigo.calc.runden(trigo.values.height, 4);
                    trigo.inputs[13].value = trigo.calc.runden(trigo.values.umfang, 4);
                    trigo.inputs[14].value = trigo.calc.runden(trigo.values.flaeche, 4);
                    trigo.renderer.draw();
                }

            }
            ww = 0;
        },
        calc: {
            pi: Math.PI,
            berechnen: function () {
                var w, s;
                var code = 100;
                w = trigo.values.h[3] + trigo.values.h[4] + trigo.values.h[5];
                s = trigo.values.h[0] + trigo.values.h[1] + trigo.values.h[2];
                if (w === 3) {
                    if (Math.abs(trigo.values.ac + trigo.values.bc + trigo.values.cc - 180) > 0.0) {
                        if (trigo.values.g[5] === 0) {
                            trigo.values.h[5] = 0;
                            w = 2;
                        }
                        if (trigo.values.g[4] === 0) {
                            trigo.values.h[4] = 0;
                            w = 2;
                        }
                        if (trigo.values.g[3] === 0) {
                            trigo.values.h[3] = 0;
                            w = 2;
                        }
                        if (w === 3) {
                            trigo.msg.print("Winkelsumme falsch (" + (trigo.values.ac + trigo.values.bc + trigo.values.cc) + ")");
                            return trigo.codes.ERROR;
                        }
                    }
                    if ((trigo.values.ac < 0.0) || (trigo.values.bc < 0.0) || (trigo.values.cc < 0.0)) {
                        trigo.msg.print("Winkel falsch (Summe zweier Winkel bereits > 180°)");
                        return trigo.codes.ERROR;
                    }
                }
                if (w === 2) {
                    if (trigo.values.h[3] === 0) {
                        trigo.values.ac = 180 - trigo.values.bc - trigo.values.cc;
                        trigo.values.h[3] = 1;
                    }
                    else if (trigo.values.h[4] === 0) {
                        trigo.values.bc = 180 - trigo.values.ac - trigo.values.cc;
                        trigo.values.h[4] = 1;
                    }
                    else if (trigo.values.h[5] === 0) {
                        trigo.values.cc = 180 - trigo.values.ac - trigo.values.bc;
                        trigo.values.h[5] = 1;
                    }
                    return trigo.codes.OK;
                }
                if ((w === 3) && (s > 0)) {
                    code = trigo.codes.NOT_FINISHED;
                    if (trigo.values.h[0] === 1) {
                        if (trigo.values.h[1] === 0) {
                            trigo.values.b = trigo.values.a * trigo.calc.sin(trigo.values.bc) / trigo.calc.sin(trigo.values.ac);
                            code = trigo.codes.OK;
                        }
                        if (trigo.values.h[2] === 0) {
                            trigo.values.c = trigo.values.a * trigo.calc.sin(trigo.values.cc) / trigo.calc.sin(trigo.values.ac);
                            code = trigo.codes.OK;
                        }
                    }
                    if (trigo.values.h[1] === 1) {
                        if (trigo.values.h[0] === 0) {
                            trigo.values.a = trigo.calc.sin(trigo.values.ac) / trigo.calc.sin(trigo.values.bc) * trigo.values.b;
                            code = trigo.codes.OK;
                        }
                        if (trigo.values.h[2] === 0) {
                            trigo.values.c = trigo.calc.sin(trigo.values.cc) / trigo.calc.sin(trigo.values.bc) * trigo.values.b;
                            code = trigo.codes.OK;
                        }
                    }
                    if (trigo.values.h[2] === 1) {
                        if (trigo.values.h[0] === 0) {
                            trigo.values.a = trigo.calc.sin(trigo.values.ac) / trigo.calc.sin(trigo.values.cc) * trigo.values.c;
                            code = trigo.codes.OK;
                        }
                        if (trigo.values.h[1] === 0) {
                            trigo.values.b = trigo.calc.sin(trigo.values.bc) / trigo.calc.sin(trigo.values.cc) * trigo.values.c;
                            code = trigo.codes.OK;
                        }
                    }
                    trigo.values.h[0] = 1;
                    trigo.values.h[1] = 1;
                    trigo.values.h[2] = 1;
                    return code;
                }
                if ((w === 1) && (s === 2)) {
                    if (trigo.values.h[0] + trigo.values.h[1] + trigo.values.h[5] === 3) {
                        trigo.values.c = Math.sqrt(trigo.values.a * trigo.values.a + trigo.values.b * trigo.values.b - 2 * trigo.values.a * trigo.values.b * trigo.calc.cos(trigo.values.cc));
                        trigo.values.h[2] = 1;
                        return trigo.codes.OK;
                    }
                    else if (trigo.values.h[1] + trigo.values.h[2] + trigo.values.h[3] === 3) {
                        trigo.values.a = Math.sqrt(trigo.values.b * trigo.values.b + trigo.values.c * trigo.values.c - 2 * trigo.values.b * trigo.values.c * trigo.calc.cos(trigo.values.ac));
                        trigo.values.h[0] = 1;
                        return trigo.codes.OK;
                    }
                    else if (trigo.values.h[0] + trigo.values.h[2] + trigo.values.h[4] === 3) {
                        trigo.values.b = Math.sqrt(trigo.values.a * trigo.values.a + trigo.values.c * trigo.values.c - 2 * trigo.values.a * trigo.values.c * trigo.calc.cos(trigo.values.bc));
                        trigo.values.h[1] = 1;
                        return trigo.codes.OK;
                    }
                    if ((trigo.values.h[0] + trigo.values.h[1] + trigo.values.h[3] === 3) && (trigo.values.a >= trigo.values.b)) {
                        trigo.values.bc = trigo.calc.asin(trigo.values.b * trigo.calc.sin(trigo.values.ac) / trigo.values.a);
                        trigo.values.h[4] = 1;
                        return trigo.codes.OK;
                    }
                    else if ((trigo.values.h[1] + trigo.values.h[2] + trigo.values.h[4] === 3) && (trigo.values.b >= trigo.values.c)) {
                        trigo.values.cc = trigo.calc.asin(trigo.values.c * trigo.calc.sin(trigo.values.bc) / trigo.values.b);
                        trigo.values.h[5] = 1;
                        return trigo.codes.OK;
                    }
                    else if ((trigo.values.h[2] + trigo.values.h[0] + trigo.values.h[5] === 3) && (trigo.values.c >= a)) {
                        trigo.values.ac = trigo.calc.asin(trigo.values.a * trigo.calc.sin(trigo.values.cc) / trigo.values.c);
                        trigo.values.h[3] = 1;
                        return trigo.codes.OK;
                    }
                    if ((trigo.values.h[0] + trigo.values.h[1] + trigo.values.h[4] === 3) && (trigo.values.a <= trigo.values.b)) {
                        trigo.values.ac = trigo.calc.asin(trigo.values.a * trigo.calc.sin(trigo.values.bc) / trigo.values.b);
                        trigo.values.h[3] = 1;
                        return trigo.codes.OK;
                    }
                    else if ((trigo.values.h[1] + trigo.values.h[2] + trigo.values.h[5] === 3) && (trigo.values.b <= trigo.values.c)) {
                        trigo.values.bc = trigo.calc.asin(trigo.values.b * trigo.calc.sin(trigo.values.cc) / trigo.values.c);
                        trigo.values.h[4] = 1;
                        return trigo.codes.OK;
                    }
                    else if ((trigo.values.h[2] + trigo.values.h[0] + trigo.values.h[3] === 3) && (trigo.values.c <= trigo.values.a)) {
                        trigo.values.cc = trigo.calc.asin(trigo.values.c * trigo.calc.sin(trigo.values.ac) / trigo.values.a);
                        trigo.values.h[5] = 1;
                        return trigo.codes.OK;
                    }
                }
                code = trigo.codes.NOT_FINISHED;
                if (s === 3) {
                    if (trigo.values.a + trigo.values.b < trigo.values.c ||
                            trigo.values.b + trigo.values.c < trigo.values.a ||
                            trigo.values.c + trigo.values.a < trigo.values.b) {
                        trigo.msg.print("Nicht Valides Dreieck");
                        return trigo.codes.ERROR;
                    }
                    if (trigo.values.h[3] === 0) {
                        trigo.values.ac = trigo.calc.acos((trigo.values.a * trigo.values.a - trigo.values.b * trigo.values.b - trigo.values.c * trigo.values.c) / (-2 * trigo.values.b * trigo.values.c));
                        code = trigo.codes.OK;
                    }
                    if (trigo.values.h[4] === 0) {
                        trigo.values.bc = trigo.calc.acos((trigo.values.b * trigo.values.b - trigo.values.c * trigo.values.c - trigo.values.a * trigo.values.a) / (-2 * trigo.values.c * trigo.values.a));
                        code = trigo.codes.OK;
                    }
                    if (trigo.values.h[5] === 0) {
                        trigo.values.cc = trigo.calc.acos((trigo.values.c * trigo.values.c - trigo.values.a * trigo.values.a - trigo.values.b * trigo.values.b) / (-2 * trigo.values.a * trigo.values.b));
                        code = trigo.codes.OK;
                    }
                    trigo.values.h[3] = 1;
                    trigo.values.h[4] = 1;
                    trigo.values.h[5] = 1;
                }
                return code;
            },
            restBerechnen: function () {
                trigo.values.ca = trigo.calc.cos(trigo.values.ac) * trigo.values.b;
                trigo.values.cb = trigo.calc.cos(trigo.values.bc) * trigo.values.a;

                if (trigo.values.ca < 0) {
                    trigo.values.width = (trigo.values.ca * -1) + trigo.values.cb;
                } else if (trigo.values.cb < 0) {
                    trigo.values.width = (trigo.values.cb * -1) + trigo.values.ca;
                } else {
                    trigo.values.width = trigo.values.cb + trigo.values.ca;
                }
                trigo.values.height = trigo.calc.sin(trigo.values.ac) * trigo.values.b;

                trigo.values.umfang = trigo.values.a + trigo.values.b + trigo.values.c;
                trigo.values.flaeche = ((trigo.values.height * trigo.values.ca) / 2) + ((trigo.values.height * trigo.values.cb) / 2);
            },
            runden: function (t, i) {
                var d = Math.pow(10, i);
                var rt = String(Math.round(t * d) / d).replace(/\./g, ".");
                if (rt.charAt(0) === ".") rt = "0" + rt;
                return rt;
            },
            asin: function (X) {
                return trigo.calc.atn(X / Math.sqrt(-X * X + 1));

            },
            acos: function (X) {
                return trigo.calc.atn(-X / Math.sqrt(-X * X + 1)) + 2 * trigo.calc.atn(1);

            },
            atn: function (X) {
                return Math.atan(X) / trigo.calc.pi * 180;

            },
            sin: function (X) {
                return Math.sin(X * trigo.calc.pi / 180);

            },
            cos: function (X) {
                return Math.cos(X * trigo.calc.pi / 180);
            },
            tan: function (X) {
                return Math.tan(X * trigo.calc.pi / 180);
            }
        },
        renderer: {
            svg: document.querySelector("#svg"),
            dimensions: {
                width: 0.0,
                height: 0.0
            },
            ctx: null,
            pos: {
                A: {
                    x: 0,
                    y: 0
                },
                B: {
                    x: 0,
                    y: 0
                },
                C: {
                    x: 0,
                    y: 0
                },

                lA: {
                    x: 0,
                    y: 0
                },
                lB: {
                    x: 0,
                    y: 0
                },
                lC: {
                    x: 0,
                    y: 0
                },

                la: {
                    x: 0,
                    y: 0
                },
                lb: {
                    x: 0,
                    y: 0
                },
                lc: {
                    x: 0,
                    y: 0
                },

                lalpha: {
                    x: 0,
                    y: 0
                },
                lbeta: {
                    x: 0,
                    y: 0
                },
                Lgama: {
                    x: 0,
                    y: 0
                }
            },
            path: {
                start: {
                    x: 0,
                    y: 0
                },
                moves: []
            },
            scale: 0,
            init: function () {
                this.dimensions.width = Number(this.svg.getAttribute("viewBox").split(" ")[2]);
                this.dimensions.height = Number(this.svg.getAttribute("viewBox").split(" ")[3]);
                this.clear();
            },
            calc: function () {
                var MAX_WIDTH = this.dimensions.width - 40;
                var MAX_HEIGHT = this.dimensions.height - 40;
                this.scale = 0;
                do {
                    this.scale += 0.0000001;
                } while (trigo.values.width / this.scale > MAX_WIDTH || trigo.values.height / this.scale > MAX_HEIGHT);
                if (trigo.values.ca < 0.0) {
                    this.pos.A.x = Number(trigo.calc.runden((trigo.values.ca * -1), 1) / this.scale);
                } else {
                    this.pos.A.x = 0;
                }
                this.pos.A.y = 0;

                this.pos.B.x = Number(trigo.calc.runden((trigo.values.c / this.scale),1));
                this.pos.B.y = 0;

                if (trigo.values.ca < 0.0) {
                    this.pos.C.x = 0;
                } else if (trigo.values.cb < 0.0) {
                    this.pos.C.x = Number(trigo.calc.runden(MAX_WIDTH,1));
                } else {
                    this.pos.C.x = Number(trigo.calc.runden(trigo.values.ca / this.scale,1));
                }
                this.pos.C.y = Number(trigo.calc.runden(trigo.values.height / this.scale, 1));

                this.calcPaths();
            },
            calcPaths: function () {
                // var MAX_WIDTH = this.dimensions.width - 40;
                var MAX_HEIGHT = this.dimensions.height - 40;

                this.path.start.x = this.pos.A.x;
                this.path.start.y = MAX_HEIGHT - this.pos.A.y;


                this.path.moves[0] = {
                    x: this.pos.B.x - this.pos.A.x,
                    y: (this.pos.B.y - this.pos.A.y) * -1
                };

                this.path.moves[1] = {
                    x: this.pos.C.x - this.pos.B.x,
                    y: (this.pos.C.y - this.pos.B.y) * -1
                };

                this.path.moves[2] = {
                    x: this.pos.A.x - this.pos.C.x,
                    y: (this.pos.A.y - this.pos.C.y) * -1
                };

                this.calcLables();
            },
            calcLables: function () {
                this.pos.lA.x = this.pos.A.x - 14;
                this.pos.lA.y = this.pos.A.y - 18;

                this.pos.lB.x = this.pos.B.x + 2;
                this.pos.lB.y = this.pos.B.y - 18;

                this.pos.lC.x = this.pos.C.x - 7;
                this.pos.lC.y = this.pos.C.y + 4;
            },
            draw: function () {
                this.calc();
                var path = "M ";
                var MAX_HEIGHT = this.dimensions.height - 40;


                // clipPath var
                // M 300,20 l 280,360 l -560,0 l 280,-360 Z
                path += (this.path.start.x + 20) + "," + (this.path.start.y + 20) + " ";
                this.path.moves.forEach(function (move) {
                    path += "l " + move.x + "," + move.y + " ";
                });
                path += "Z";

                this.svg.querySelector("#dreieck").setAttribute("d", path);
                this.svg.querySelector("#clipDreieck > path").setAttribute("d", path);
                //setCircle

                this.svg.querySelector("#circles > #cA").setAttribute("cx", ""+(this.pos.A.x + 20));
                this.svg.querySelector("#circles > #cA").setAttribute("cy", ""+((MAX_HEIGHT - this.pos.A.y) + 20));

                this.svg.querySelector("#circles > #cB").setAttribute("cx", ""+(this.pos.B.x + 20));
                this.svg.querySelector("#circles > #cB").setAttribute("cy", ""+((MAX_HEIGHT - this.pos.B.y) + 20));

                this.svg.querySelector("#circles > #cC").setAttribute("cx", ""+(this.pos.C.x + 20));
                this.svg.querySelector("#circles > #cC").setAttribute("cy", ""+((MAX_HEIGHT - this.pos.C.y) + 20));


                this.svg.querySelector("#lA").setAttribute("x", ""+(this.pos.lA.x + 20));
                this.svg.querySelector("#lA").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lA.y) + 20));

                this.svg.querySelector("#lB").setAttribute("x", ""+(this.pos.lB.x + 20));
                this.svg.querySelector("#lB").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lB.y) + 20));

                this.svg.querySelector("#lC").setAttribute("x", ""+(this.pos.lC.x + 20));
                this.svg.querySelector("#lC").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lC.y) + 20));


                // this.svg.querySelector("#lalpha").setAttribute("x", ""+(this.pos.lA.x + 20));
                // this.svg.querySelector("#lalpha").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lA.y) + 20));
                //
                // this.svg.querySelector("#lbeta").setAttribute("x", ""+(this.pos.lB.x + 20));
                // this.svg.querySelector("#lbeta").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lB.y) + 20));
                //
                // this.svg.querySelector("#lgama").setAttribute("x", ""+(this.pos.lC.x + 20));
                // this.svg.querySelector("#lgama").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lC.y) + 20));
                //
                //
                // this.svg.querySelector("#lA").setAttribute("x", ""+(this.pos.lA.x + 20));
                // this.svg.querySelector("#lA").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lA.y) + 20));
                //
                // this.svg.querySelector("#lB").setAttribute("x", ""+(this.pos.lB.x + 20));
                // this.svg.querySelector("#lB").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lB.y) + 20));
                //
                // this.svg.querySelector("#lC").setAttribute("x", ""+(this.pos.lC.x + 20));
                // this.svg.querySelector("#lC").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lC.y) + 20));

            },
            clear: function () {
                var MAX_HEIGHT = this.dimensions.height - 40;
                this.pos.B.x = 549.3;

                this.pos.C.x = 252;
                this.pos.C.y = 360;

                this.calcPaths();

                var path = "M ";

                path += (this.path.start.x + 20) + "," + (this.path.start.y + 20) + " ";
                this.path.moves.forEach(function (move) {
                    path += "l " + move.x + "," + move.y + " ";
                });
                path += "Z";

                this.svg.querySelector("#dreieck").setAttribute("d", path);
                this.svg.querySelector("#clipDreieck > path").setAttribute("d", path);

                this.svg.querySelector("#circles > #cA").setAttribute("cx", ""+(this.pos.A.x + 20));
                this.svg.querySelector("#circles > #cA").setAttribute("cy", ""+((MAX_HEIGHT - this.pos.A.y) + 20));

                this.svg.querySelector("#circles > #cB").setAttribute("cx", ""+(this.pos.B.x + 20));
                this.svg.querySelector("#circles > #cB").setAttribute("cy", ""+((MAX_HEIGHT - this.pos.B.y) + 20));

                this.svg.querySelector("#circles > #cC").setAttribute("cx", ""+(this.pos.C.x + 20));
                this.svg.querySelector("#circles > #cC").setAttribute("cy", ""+((MAX_HEIGHT - this.pos.C.y) + 20));


                this.svg.querySelector("#lA").setAttribute("x", ""+(this.pos.lA.x + 20));
                this.svg.querySelector("#lA").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lA.y) + 20));

                this.svg.querySelector("#lB").setAttribute("x", ""+(this.pos.lB.x + 20));
                this.svg.querySelector("#lB").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lB.y) + 20));

                this.svg.querySelector("#lC").setAttribute("x", ""+(this.pos.lC.x + 20));
                this.svg.querySelector("#lC").setAttribute("y", ""+((MAX_HEIGHT - this.pos.lC.y) + 20));
            }
        },
        msg: {
            msgList: document.querySelector("#msglist"),
            levels: "",
            objs: [],
            // <li class="msg">
            //     <span>msg</span>
            // </li>
            print: function (msg) {
                var li = document.createElement("li");
                li.classList.add("msg");
                var span = document.createElement("span");
                span.innerHTML = msg;
                li.appendChild(span);
                this.msgList.appendChild(li);
                // console.log(msg);
            }
            // ,
            // print: function (msg, descr) {
            //     var li = document.createElement("li");
            //     li.classList.add("msg");
            //     var span = document.createElement("span");
            //     span.innerHTML = msg;
            //     li.appendChild(span);
            //     this.msgList.appendChild(li);
            //     // console.log(msg);
            // }
        }
    };

trigo.init();